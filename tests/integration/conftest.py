import logging
import pytest
import random
import shutil
import string

log = logging.getLogger(__name__)

if not shutil.which("vagrant"):
    pytest.skip(
        "The 'vagrant' command is required to run these tests", allow_module_level=True
    )


@pytest.fixture
def hub(hub):
    hub.log.debug("Initializing hub")
    hub.pop.sub.add("idem.idem")
    yield hub


@pytest.fixture(scope="module")
def instance_name():
    yield "test_idem_cloud_" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )
